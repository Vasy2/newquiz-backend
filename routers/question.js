var express = require('express');
//var Item = require('../models/item');

var Question = require('../models/question');

//var itemRouter = express.Router();

var questionRouter = express.Router();

questionRouter
  .route('/questions')
  .post(function (request, response) {

    console.log('POST /questions');

    var question = new Question(request.body);

    question.save();

    response.status(201).send(question);
  })
  .get(function (request, response) {

    console.log('GET /questions');

    Question.find(function (error, questions) {

      if (error) {
        response.status(500).send(error);
        return;
      }

      console.log(questions);

      response.json(questions);
    });
  });

questionRouter
  .route('/questions/:questionId')
  .get(function (request, response) {

    console.log('GET /questions/:questionId');

    var questionId = request.params.questionId;

    Question.findOne({
      id: questionId
    }, function (error, question) {

      if (error) {
        response.status(500).send(error);
        return;
      }

      console.log(question);

      response.json(question);

    });
  })
  .put(function (request, response) {

    console.log('PUT /questions/:questionId');

    var questionId = request.params.questionId;

    Question.findOne({
      id: questionId
    }, function (error, question) {

      if (error) {
        response.status(500).send(error);
        return;
      }

      if (question) {
        question.text = request.body.text;
        question.character = request.body.character;
        question.type = request.body.type;
        question.crazy = request.body.crazy;
        question.options = request.body.options;

        question.save();

        response.json(question);
        return;
      }

      response.status(404).json({
        message: 'Question with id ' + questionId + ' was not found.'
      });
    });
  })
  .patch(function (request, response) {

    console.log('PATCH /questions/:questionId');

    var questionId = request.params.questionId;

    Question.findOne({
      id: questionId
    }, function (error, question) {

      if (error) {
        response.status(500).send(error);
        return;
      }

      if (question) {

        for (var property in request.body) {
          if (request.body.hasOwnProperty(property)) {
            if (typeof question[property] !== 'undefined') {
              question[property] = request.body[property];
            }
          }
        }

        // if (request.body.name) {
        //   item.name = request.body.name;
        // }

        // if (request.body.description) {
        //   item.description = request.body.description;
        // }

        // if (request.body.quantity) {
        //   item.quantity = request.body.quantity;
        // }

        question.save();

        response.json(question);
        return;
      }

      response.status(404).json({
        message: 'Question with id ' + questionId + ' was not found.'
      });
    });
  })
  .delete(function (request, response) {

    console.log('DELETE /questions/:questionId');

    var questionId = request.params.questionId;

    Question.findOne({
      id: questionId
    }, function (error, question) {

      if (error) {
        response.status(500).send(error);
        return;
      }

      if (question) {
        question.remove(function (error) {

          if (error) {
            response.status(500).send(error);
            return;
          }

          response.status(200).json({
            'message': 'Question with id ' + questionId + ' was removed.'
          });
        });
      } else {
        response.status(404).json({
          message: 'Question with id ' + questionId + ' was not found.'
        });
      }
    });
  });

module.exports = questionRouter;