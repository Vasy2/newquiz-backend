# RESTful API with Node.js, Express.js, Mongoose and MongoDB for the NewQuiz app.

## Install

`npm install`

## Run

0. Start MongoDB.
1. `npm run start`
