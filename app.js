var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var questionRouter = require('./routers/question');

var app = express();

var PORT = 8080;
var HOST_NAME = 'localhost';
var DATABASE_NAME = 'questions';

mongoose.connect('mongodb://' + HOST_NAME + '/' + DATABASE_NAME);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/api', questionRouter);

app.listen(PORT, function () {
  console.log('Listening on port ' + PORT);
});
