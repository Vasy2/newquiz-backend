var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questionSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true
  },
  text: {
    type: String,
    required: true,
  },
  character: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  crazy: {
    type: Boolean
  },
  options: {},

}, {
  collection: 'dataItems'
});

module.exports = mongoose.model('Question', questionSchema);